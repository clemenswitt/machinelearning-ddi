const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const path = require('path');

let menuTemplate = [
  {
      label: "ML-App",
      submenu: [
          {
            label: "Über diese App",
            role:  "about"
          },
          {
            label: "Schließen",
            role: "quit"
          }
      ]
  },
];


// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 695,
    autoHideMenuBar: true,
    icon: __dirname + '/icon.ico',
    title: 'Machine-Learning App',
    webPreferences: {
      webviewTag: true,
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  //dev-tools öffnen
  //mainWindow.webContents.openDevTools();

  //Main-Window auf feste Größe begrenzen
  mainWindow.setResizable(false);

  //Menü aus menuTemplate erstellen
  const mainMenu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(mainMenu);

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

  //Local-Storage bei App-Start löschen
  mainWindow.webContents.session.clearStorageData();

  //Seite des MainWindow durch beliebiges JS-Skript wechseln
  ipcMain.on('change-file', (event, fileName) => {
    //Inhalt wechseln
    mainWindow.loadFile(path.join(__dirname, fileName));
    //
    mainWindow.show();
  });

  //Bestimmten FAQ-Artikel in MainWindow Anzeigen
  ipcMain.on('open-faq-article', (event, articleName) => {
    //FAQ-Seite in MainWindow anzeigen wechseln
    mainWindow.loadFile(path.join(__dirname, 'faq.html'));
    //JS-Code zum Anzeigen des entsprechenden mdc-dialogs generieren (--> scripts/faq.js)
    var openArticleCode = "openDialog(\'" + articleName + "\')";
    //Generierten JS-Code in MainWindow injizieren
    mainWindow.webContents.executeJavaScript(openArticleCode);
    //MainWindow nach vorn bringen
    mainWindow.show();
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);


// Quit when all windows are closed.
app.on('window-all-closed', () => {
  app.quit();
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

