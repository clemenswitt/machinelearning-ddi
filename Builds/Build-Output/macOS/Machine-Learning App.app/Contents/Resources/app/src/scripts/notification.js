//ipcRenderer importieren --> Kommunikation mit main-Prozess
const { ipcRenderer } = require("electron");

//Sleep
function Sleep(milliseconds) {
   return new Promise(resolve => setTimeout(resolve, milliseconds));
}

//Benachrichtigung erstellen
async function createFAQNotification(title, text, articleID) {
   var notificationTitle = title;
   var notificationText = text;
   var notificationArticle = articleID;

   notification = new Notification(title, {body: text, icon: '../frontend-contents/images/camera-icon.png', silent: true});

   //Bei Klick auf Benachrichtigung angezeigte Seite des Main-Fensters ändern
   notification.addEventListener('click', (event) => {
      //open-faq-article -- Listener des Main-Prozesses triggern --> Argument = ID des anzuzeigenden Artikels
      ipcRenderer.send('open-faq-article', notificationArticle);   
   });
}

async function sendNotifications() {
   await Sleep(300000);
   createFAQNotification('Brauchst du Hilfe?', 'Klicke hier, um hilfreiche Tipps zum Erstellen von Trainingsdaten zu erhalten.', 'help-training');
   await Sleep(600000);
   createFAQNotification('Projekt speichern?', 'Klicke, um zu erfahren, wie du dein trainiertes Modell speicherst!', 'help-save-model');
}