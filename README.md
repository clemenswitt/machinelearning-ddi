![eduinf-logo](app/src/frontend-contents/images/eduinf.png "EduInf-Logo")

# Download 
- [Windows](https://gitlab.com/clemenswitt/machinelearning-ddi/-/blob/dev/Builds/DOWNLOAD/Windows.zip)
- [macOS](https://gitlab.com/clemenswitt/machinelearning-ddi/-/blob/dev/Builds/DOWNLOAD/macOS.zip) 


# Build

## Windows
```
electron-packager . ml-app --overwrite --asar=true --platform=win32 --arch=ia32 --icon=assets/icons/win/icon.ico --prune=true --out=release-builds --version-string.CompanyName=CE --version-string.FileDescription=CE --version-string.ProductName="Machine-Learning App"
```


## macOS
```
electron-packager . --overwrite --platform=darwin --ignore="^/node_modules/app-builder-bin" --arch=x64 --icon=assets/icons/mac/icon.icns --prune=true --out=release-builds
```


# Benötigte Ressourcen
- [git](https://git-scm.com/)
- [nodeJS](https://nodejs.org/de/)
- ElectronJS: `npm install electron -g`
- Electron Forge: `npm install electron-forge -g`

- Für Neuinstallationen: `create-electron-app <name>`