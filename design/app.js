//Top-App-Bar
import { MDCTopAppBar } from '@material/top-app-bar';
const topAppBarElement = document.querySelector('.mdc-top-app-bar');
const topAppBar = new MDCTopAppBar(topAppBarElement);


//Menus
import { MDCMenu } from '@material/menu';
const mainMenu = new MDCMenu(document.getElementById('main-menu'));
mainMenu.setAbsolutePosition(10, 75);
mainMenu.open = false;

const helpMenu = new MDCMenu(document.getElementById('help-menu'));
helpMenu.setAbsolutePosition(window.innerWidth - document.getElementById('help-menu').style.width - 30, 75);
helpMenu.open = false;

function openMainMenu() {
    mainMenu.open = true;
}
document.getElementById('menu-button').addEventListener("click", openMainMenu);

function openHelpMenu() {
    helpMenu.open = true;
}
document.getElementById('help-button').addEventListener("click", openHelpMenu);

//Lists
import {MDCList} from '@material/list';
const list = new MDCList(document.querySelector('.mdc-list'));

//Dialogs
import {MDCDialog} from '@material/dialog';
const dialog = new MDCDialog(document.querySelector('.mdc-dialog')); 

//Ripple-Effects
import { MDCRipple } from '@material/ripple';
//mdc-button
const buttonRipple = new MDCRipple(document.querySelector('.mdc-button'));
//mdc-icon-button
const iconButtonRipple = new MDCRipple(document.querySelector('.mdc-icon-button'));
iconButtonRipple.unbounded = true;
//mdc-card
const selector = '.mdc-button, .mdc-icon-button, .mdc-card__primary-action';
const ripples = [].map.call(document.querySelectorAll(selector), function (el) {
    return new MDCRipple(el);
});
//mdc-list
const listItemRipples = list.listElements.map((listItemEl) => new MDCRipple(listItemEl));