function Sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

async function fadeLogo() {
    await Sleep(2000);
    document.getElementById('eduinf-logo-wrapper').style.animation = 'fade-eduinf-logo 1s';
    await Sleep(1000);
    document.getElementById('eduinf-logo-wrapper').style.display = 'none';
}