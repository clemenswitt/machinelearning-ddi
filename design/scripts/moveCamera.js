//Fenstergröße
const w = 1000;
const h = 600;

function changeCameraAngle(event) {
    var x = event.clientX;
    var y = event.clientY;

    var dX = w - x;
    var dY = h - y;

    var deg =  Math.atan(dY / dX) * (180 / Math.PI);
    var rot = deg + 20

    if (deg > -20 && deg < 90 && !(x > 800 && y > 400)) {
        document.getElementById('mascot').style.transform = 'rotate(' + rot + 'deg) scaleX(-1)';
    }
}