//Event-Listener esc-Taste
window.addEventListener('keydown', (event) => {
    if (event.keyCode == 27) {
        closeDialogs();
    }
});


//open dialog
function openDialog(dialogID) {
    document.getElementById(dialogID).classList.add('mdc-dialog--open');

    document.querySelectorAll('.mdc-dialog').forEach(item => item.addEventListener('click', (event) => {closeDialogs();}));
}

//close dialogs
function closeDialogs() {
    document.querySelectorAll('.mdc-dialog').forEach(item => item.classList.remove('mdc-dialog--open'));
}
