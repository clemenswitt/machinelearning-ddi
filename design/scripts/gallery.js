//Event-Listener zur Abfrage der Tastatureingaben
window.addEventListener('keydown', (event) => {
    if (event.keyCode == 37) {
        changeImage(-1);
    } else if (event.keyCode == 39) {
        changeImage(1);
    }
});


//Galerie initialisieren
//location -> Directory der Bilddateien
//frames   -> Anzahl der anzuzeigenden Bilder
function initializeGallery(location, frames) {
    imageLocation = location;
    numberOfFrames = frames;
    var imageURL = "url('" + imageLocation + "1.jpg')";

    //Nummer des aktuellen Frame in local-storage schreiben
    localStorage.setItem('image-number', 1);

    //Punkte zur Anzeige der aktuellen Position hinzufügen
    addProgressDots();

    //Erstes Bild anzeigen
    document. getElementById('gallery-image-container').style.backgroundImage = imageURL;
}

//Positionspunkte (unten) hinzufügen
function addProgressDots() {
    for(var i = 1; i <= numberOfFrames; i++) {
        var progressDot = document.createElement('progress-dot');
        progressDot.classList.add('progress-dot');
        progressDot.id = 'progress-dot-' + i;
        document.getElementById('progress-indicator').appendChild(progressDot);
        document.getElementById('progress-dot-1').style.transform = 'scale(2)';
    }
}

//Wechsel der Frames
//direction = 1  -> vorwärts
//direction = -1 -> rückwärts
function changeImage(direction) {
    //Nächsten anzuzeigenden Frame definieren
    var imageNumber = parseInt(localStorage.getItem('image-number')) + direction;

    //Navigationsgrenzen einhalten
    if (imageNumber < 1) {
        imageNumber = 1;
    } else if (imageNumber > numberOfFrames) {
        imageNumber = numberOfFrames;
    }

    //Styling des letten Positionspunkts zurücksetzen
    document.getElementById('progress-dot-' + (imageNumber - direction)).style.transform = 'scale(1)';
    //Aktuellen Positionspunkt hervorheben
    document.getElementById('progress-dot-' + imageNumber).style.transform = 'scale(2)';

    //Aktuelle Anzeigeposition in local-storage aktualisieren
    localStorage.removeItem('image-number');
    localStorage.setItem('image-number', imageNumber);
    
    //Aktuellen Frame definieren und anzeigen
    var imageURL = "url('" + imageLocation + imageNumber + ".jpg')";
    document. getElementById('gallery-image-container').style.backgroundImage = imageURL;
}
