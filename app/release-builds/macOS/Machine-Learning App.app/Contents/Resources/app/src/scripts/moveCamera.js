//Kameraposition
const w = 1000;
const h = 510;

function changeCameraAngle(event) {
    //Aktuelle Mausposition
    var x = event.clientX;
    var y = event.clientY;

    //Abstand zur Kamera
    var dX = w - x;
    var dY = h - y;

    //Winkel ermitteln
    var deg =  Math.atan(dY / dX) * (180 / Math.PI);
    //Offset: +20deg
    var rot = deg + 20

    //Kamerawinkel anpassen, wenn Mausposition nicht exakt über Kamera (Ruckeln vermeiden)
    if (deg > -20 && deg < 90 && !(x > 800 && y > 400)) {
        document.getElementById('mascot').style.transform = 'rotate(' + rot + 'deg) scaleX(-1)';
    }
}