//Sleep
function Sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}


async function fadeLogo() {
    //Startbildschirm bereits gezeigt?
    if (localStorage.getItem('fade-done') == null) {
        //Falls nein, Bit setzen & Startbildschirm + fade anzeigen
        // --> Bit durch clearStorageData() bei App-Start gelöscht
        localStorage.setItem('fade-done', 1)
        console.log(localStorage.getItem('fade-done'));
        await Sleep(2000);
        document.getElementById('eduinf-logo-wrapper').style.animation = 'fade-eduinf-logo 1s';
        await Sleep(1000);
        document.getElementById('eduinf-logo-wrapper').style.display = 'none';
    //Falls Startbildschirm bereits angezeigt (Bit gesetzt) --> Startbildschirm unsichtbar machen
    } else {
        document.getElementById('eduinf-logo-wrapper').style.display = 'none';
    }
}