// URL des TeachableMachine-Modells
const URL = "https://teachablemachine.withgoogle.com/models/4ch2Bb3e8/";

let model, webcam, labelContainer, maxPredictions;

// Initialisierung
async function init() {
    const modelURL = URL + "model.json";
    const metadataURL = URL + "metadata.json";

    // Trainiertes TeachableMachine-Modell laden
    model = await tmImage.load(modelURL, metadataURL);
    maxPredictions = model.getTotalClasses(); //max. Anzahl Vorhersagen = Anzahl der trainierten Gesten

    // Webcam durch Klasse tmImage initialisieren
    const flip = true; // Speiegelung des Webcam-Bilds
    webcam = new tmImage.Webcam(500, 500, flip); // Höhe, Breite, Spiegelung
    await webcam.setup(); // Webcam starten
    await webcam.play();
    window.requestAnimationFrame(loop);

    // Webcam-Output vorgesehenem DIV hinzufügen
    document.getElementById("webcam-container").appendChild(webcam.canvas);
    
}

async function loop() {
    webcam.update(); // Webcam updaten
    await predict(); // Bewertung des aktuellen Webcam-Bilds durch TM-Modell initiieren
    window.requestAnimationFrame(loop);

    //500ms warten
    await Sleep(500);
}

// Aktuelles Webcam-Bild durch TM-Modell laufen lassen
var predictionClassNames = []   // Namen der angelegten Gesten-Klassen
var predictionData = [];        // Bewertungen der einzelnen Gesten
var predictionResult;           // Wahrscheinlichste Geste
async function predict() {
    // Webcam-Bild in Modell eingeben
    const prediction = await model.predict(webcam.canvas);
    
    // Erhaltenes JSON-Object in einzelne Gesten-Klassen und Vorhersagewerte aufteilen
    for(let i = 0; i < maxPredictions; i++) {
        predictionClassNames[i] = prediction[i]['className'];
        predictionData[i] = prediction[i]['probability'];
    }

    // Maximum aus predictionData ermitteln -> Index des Maximums ermitteln -> Korrespondierenden Gesten-Klassen-Namen aus predictionClassNames ermitteln
    predictionResult = predictionClassNames[predictionData.indexOf(Math.max(...predictionData))];
    //console.log(predictionResult);
    document.getElementById('prediction-label').innerHTML = predictionResult;

    //Cookie fadedIn --> fadeIn-Animation nur einmal ausführen

    if (predictionResult == 'Hallo!') {
        if (localStorage.getItem('fadedIn') == null) {
            fadeInHand();
            localStorage.setItem('fadedIn', 1);
        }
    } else {
        if (localStorage.getItem('fadedIn') != null) {
            fadeOutHand();
            localStorage.removeItem('fadedIn');
        }
    }
}

//Sleep
function Sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

//Fade in Hand
function fadeInHand() {
    document.getElementById('waving-hand').style.display = 'block';
    document.getElementById('waving-hand').style.animation = 'fade-in-hand 0.3s';
}

//Fade out Hand
async function fadeOutHand() {
    document.getElementById('waving-hand').style.animation = 'fade-out-hand 0.3s';
    
    await Sleep(300);

    document.getElementById('waving-hand').style.display = 'none';
}
